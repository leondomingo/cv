# León Domingo
## Fullstack webdeveloper

Full-stack web developer and enthusiast of **the WWW** universe for its freedom of movement.

I'm in love with **open-source projects** which give us a great support to create useful things with a low cost. (Almost) every tool I use is open source (Linux, Python, PostgreSQL, VSCode, etc, etc) and I (usually) do not like propietary solutions (oracle, sql server, windows, etc) because of the obligation to adopt another set of private related technologies from the same company (most of the time).

I love **Python** programming language and the whole living ecosystem around it. Great work of a lot of people. Great on-line resources and awesome performance.

I'm also focused on **JS**-related technologies: **NodeJS**, **Vue.js**, **ReactJS**, etc, with Python (or not) server side technologies.

Testing (and playing with) new technologies, as they come up, is something I like a lot. **Golang** or **Universal JS** apps with SSR are areas where I want to get better.

I used to work on desktop apps environment (Windows) but now I know I'll never come back. The WWW is the future (and the present, in fact), and we have a lot of opportunities out there.
